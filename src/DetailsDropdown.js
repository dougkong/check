import React, {Component} from 'react';
import { connect } from 'react-redux'
import styles from './detailsdropdown.css';
import {changePage} from "./actions";
import {bindActionCreators} from "redux";

class DetailsDropdown extends Component {
  render() {
    return (
      <div className="dropdown">
        <button className="dropbtn">{this.props.page} reports{' ('}
          {this.props.page === 'Pending' && this.props.pendingCandidatesCount}
          {this.props.page === 'Consider' && this.props.considerCandidatesCount}
          {this.props.page === 'On Hold' && this.props.onHoldCandidatesCount}
          {') '}<i className='down-arrow'></i>
        </button>

        <div className="dropdown-content">
          <div className='dropdown-title-row'>Personal Lists</div>
          <div className='dropdown-row' onClick={()=> {this.props.changePage('Pending')}}>
            Pending reports{' ('}{this.props.pendingCandidatesCount}{')'}</div>
          <div className='dropdown-row' onClick={()=> {this.props.changePage('Consider')}}>
            Consider reports{' ('}{this.props.considerCandidatesCount}{')'}</div>
          <div className='dropdown-row' onClick={()=> {this.props.changePage('On Hold')}}>
            On Hold reports{' ('}{this.props.onHoldCandidatesCount}{')'}</div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    page: state.page,
    pendingCandidatesCount: state.pendingCandidates.length,
    considerCandidatesCount: state.considerCandidates.length,
    onHoldCandidatesCount: state.onHoldCandidates.length
  }
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    changePage
  }, dispatch);
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DetailsDropdown);