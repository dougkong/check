export const dateDifference = (firstDateString, secondDateString) => {
  const oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
  const firstDate = new Date(firstDateString).getTime()/oneDay;
  const secondDate = new Date(secondDateString).getTime()/oneDay;

  const floorDifferenceDays = Math.floor(firstDate) - Math.ceil(secondDate);
  const ceilingDifferenceDays = Math.ceil(firstDate) - Math.floor(secondDate);

  return `${floorDifferenceDays} to ${ceilingDifferenceDays} days`;
};