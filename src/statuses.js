export const statuses = {
  'Pending': 'pending',
  'Consider': 'consider',
  'On Hold': 'suspended'
};