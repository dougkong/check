export const CHANGE_PAGE = 'CHANGE_PAGE';
export const SET_CANDIDATES = 'SET_CANDIDATES';

export const changePage = (page) => {
  return {
    type: CHANGE_PAGE,
    value: page
  }
};

export const setCandidates = (candidates, status) => {
  return {
    type: SET_CANDIDATES,
    candidates: candidates,
    status: status
  }
};