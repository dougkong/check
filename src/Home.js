import React from 'react';
import Card from './Card';
import styles from './home.css';

export default ({candidates}) => {
  return (
    <div className='container'>
      <div className='cards'>
        <Card status='Pending'
              title={{name: "NAME", status: "ESTIMATED DELIVERY"}}
              candidates={candidates.pending}/>
        <Card status='Consider'
              title={{name: "NAME", status: "UPDATED"}}
              candidates={candidates.consider} />
        <Card status='On Hold'
              title={{name: "NAME", status: "UPDATED"}}
              candidates={candidates.onHold}/>
      </div>
    </div>
  )
}