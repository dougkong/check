import React, { Component } from 'react';
import Content from './Content';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {changePage, setCandidates} from "./actions";
import {pendingCandidates, considerCandidates, onHoldCandidates} from "./mockData";

class App extends Component {
  componentWillMount() {
    this.props.setCandidates(pendingCandidates, 'Pending');
    this.props.setCandidates(considerCandidates, 'Consider');
    this.props.setCandidates(onHoldCandidates, 'On Hold');
  }

  render() {
    return (
      <div>
        <div className='header'>Checkr
          {' '}-{' '}
          <span onClick={()=> {this.props.changePage('home')}}>Home</span>
          {this.props.page !== 'home' && <span> > Lists</span>}
        </div>
        <Content />
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    page: state.page
  }
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    changePage,
    setCandidates
  }, dispatch);
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
