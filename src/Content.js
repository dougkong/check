import React, { Component } from 'react';
import { connect } from 'react-redux'
import Home from './Home';
import Details from './Details';
import { bindActionCreators } from 'redux'

class Content extends Component {
  render() {
    return (<div>
        {this.props.page === 'home' && <Home candidates={{
          'pending': this.props.pendingCandidates,
          'consider': this.props.considerCandidates,
          'onHold': this.props.onHoldCandidates
        }}/>}

        {this.props.page === 'Pending' && <Details status='Pending' candidates={this.props.pendingCandidates}/>}
        {this.props.page === 'Consider' && <Details status='Consider' candidates={this.props.considerCandidates}/>}
        {this.props.page === 'On Hold' && <Details status='On Hold' candidates={this.props.onHoldCandidates}/>}
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    page: state.page,
    pendingCandidates: state.pendingCandidates,
    considerCandidates: state.considerCandidates,
    onHoldCandidates: state.onHoldCandidates
  }
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
  }, dispatch);
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Content);