import React, {Component} from 'react';
import { connect } from 'react-redux'
import styles from './card.css';
import {changePage} from "./actions";
import {bindActionCreators} from "redux";
import { dateDifference } from "./dateUtility";
import { statuses} from "./statuses";

class Card extends Component {
  render() {
    return (
      <div className='card'>
        <div className='candidate-count'>{this.props.candidates.length}</div>
        <div className='status'>
          {this.props.status}
        </div>
        <div className='row'>
          <span className='title-row'>
            {this.props.title.name}
          </span>
          <span className={'title-row'}>
            {this.props.title.status}
          </span>
        </div>

        {
          this.props.candidates.map((candidate) => {
            return candidate.reports.map((report) => {
              return (
                report.status === statuses[this.props.status] && <div key={`${candidate.name}_${candidate.id}_${report.id}`} className='row'>
                  <span className='candidate-name'>{candidate.name}</span>
                  {this.props.status === 'Pending' && <span
                    className='candidate-status'>{dateDifference(report.estimated_completed_at, report.updated_at)}</span>}
                  {(this.props.status === 'Consider' || this.props.status === 'On Hold') && <span
                    className='candidate-status'>{new Date(report.updated_at).toLocaleDateString("en-US")}</span>}
                </div>
              )
            });
          })
        }
        <div className='footer' onClick={() => {
          this.props.changePage(this.props.status);
        }}>Show {status} reports</div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {}
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    changePage
  }, dispatch);
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Card);