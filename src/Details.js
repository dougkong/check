import React, {Component} from 'react';
import { connect } from 'react-redux'
import styles from './details.css';
import {changePage} from "./actions";
import {bindActionCreators} from "redux";
import {dateDifference} from "./dateUtility";
import {statuses} from "./statuses";
import DetailsDropdown from './DetailsDropdown';

class Details extends Component {
  render() {
    return (
      <div className='container'>
        <DetailsDropdown/>
        <div className='details'>
          <div className='detail-row'>
            <span className='detail-title-row'>
              STATUS
            </span>
            <span className={'detail-title-row'}>
              NAME
            </span>
            <span className={'detail-title-row'}>
              ESTIMATED DELIVERY
            </span>
            <span className={'detail-title-row'}>
              PACKAGE
            </span>
            <span className={'detail-title-row'}>
              GEO
            </span>
          </div>

          {
            this.props.candidates.map((candidate) => {
              return candidate.reports.map((report) => {
                return (
                  report.status === statuses[this.props.status] && <div key={candidate.name + candidate.id} className='detail-row'>
                    <span className='detail-status'>{this.props.status}</span>
                    <span className='detail-name'>{candidate.name}</span>
                    {this.props.status === 'Pending'
                    && <span className='detail-status'>
                      {dateDifference(report.estimated_completed_at, report.updated_at)}</span>}
                    {(this.props.status === 'Consider' || this.props.status === 'On Hold')
                    && <span className='detail-status'>
                      {new Date(report.updated_at).toLocaleDateString("en-US")}</span>}
                    <span className='detail-package'>{report.package}</span>
                    <span className='detail-geo'>{report.geo}</span>
                  </div>
                )
              });
            })
          }
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {}
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    changePage
  }, dispatch);
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Details);
