import {CHANGE_PAGE, SET_CANDIDATES} from "../actions";

const initialState = {
  page: 'home',
  pendingCandidates: [],
  considerCandidates: [],
  onHoldCandidates: []
};

export default (state = initialState, action) => {
  switch(action.type) {
    case CHANGE_PAGE:
      return {
        ...state,
        page: action.value
      };
    case SET_CANDIDATES:
      if (action.status === 'Pending') {
        return {
          ...state,
          pendingCandidates: action.candidates
        }
      } else if (action.status === 'Consider') {
        return {
          ...state,
          considerCandidates: action.candidates
        };
      } else if (action.status === 'On Hold') {
        return {
          ...state,
          onHoldCandidates: action.candidates
        };
      }
    default:
      return state;
  }
}